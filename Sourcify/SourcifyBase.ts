import { Innards, convertInnardsToElements } from '../El-Tool/El-Tool';
import { SourceProxyManager } from './SourceProxyManager';
import { RMapFn, RkMapFn, AnyRMapFn, ISourceContainer } from '../Derivations/Sources';


export interface ISourceProxyExits<TYPE> {
  _manager: SourceProxyManager<TYPE>,
  _static: TYPE | undefined,
}
export type SourceProxy<TYPE> = ISourceProxyExits<TYPE> & TYPE;
export type ContainerOrProxy<TYPE> = SourceProxy<TYPE> | TYPE;


/** Takes an ordinary object and makes it act like a recursive source.
 * Changing any property will update any deriver which accessed
 * that property or a child of it.
 * @importance 22 */
export function Sourcify<TYPE extends Object>(modMe: TYPE): SourceProxy<TYPE> & ISourceContainer<any, any>
export function Sourcify<TYPE>(modMe: TYPE): SourceProxy<TYPE> {
  if (typeof modMe === "object") {
    // return new SourceObjectProxy<TYPE>(() => modMe) as any;
    return new SourceProxyManager<TYPE>(() => modMe).getProxy();
  }
}




export const mapReplacers = rmap;
export function rmap<TYPE>(obj: Array<TYPE>, renderFn: RMapFn<TYPE>): ChildNode[];
export function rmap(obj: ContainerOrProxy<Object>, renderFn: RMapFn<any>): ChildNode[];
export function rmap<TYPE>(obj: SourceProxy<Array<TYPE>>, renderFn: RMapFn<TYPE>): ChildNode[];
/**
 * Stands for render-map.  Efficiently renders and manages every child of an object or array.
 * This function renders index-less items, which means they will be moved if their index changes but they will not be re-rendered.
 * @importance 18
 * @param.obj Anything which has children that can be iterated over.  Can be an array, an object, or a proxy.
 * @param.renderFn This function will get called for every child.  It only takes one argument which is the child
 * @eg const list = Sourcify(["a", "b", "c", "d"]);\
 * const listEl = div("List", rmap(list, (value) => \
 *   div("ListValue", value),\
 * ));\
 * 
 * const obj = Sourcify({ a: 1, b: 2, c: 3});\
 * const objEl = div("Obj", rmap(obj, (value) => \
 *   div("ObjValue", value),\
 * ));\
 */
export function rmap(
  obj: ContainerOrProxy<Object | Array<any>>, 
  renderFn: RMapFn<any>
): ChildNode[] {
  return _anyRMap(false, obj, renderFn);
}


export const mapKeyedReplacers = rkmap;
export function rkmap(obj: ContainerOrProxy<Object>, renderFn: RkMapFn<string, any>): ChildNode[];
export function rkmap<TYPE = any>(obj: ContainerOrProxy<Array<TYPE>>, renderFn: RkMapFn<number, TYPE>): ChildNode[];
/**
 * Stands for render-keyed-map.  Efficiently renders and manages every child of an object or array.  This
 * function is key/index dependent, so changes to an items index will cause it to be rendered from scratch.  Only use
 * this function if you need to display the index somehow.  Othewise, rmap() can handle rerendering by reordering
 * previous renders.
 * @importance 18
 * @param.obj Anything which has children that can be iterated over.  Can be an array, an object, or a proxy.
 * @param.renderFn This function will get called for every child.  It will recieve both the child and the key or index of the child.
 * @eg const list = Sourcify(["a", "b", "c", "d"]);\
 * const listEl = div("List", rkmap(list, (value, index) => \
 *   div("ListValue", `${value} is ${ index%2 ? "odd" : "even" }`),\
 * ));\
 * 
 * const obj = Sourcify({ a: 1, b: 2, c: 3});\
 * const objEl = div("Obj", rmap(obj, (value, key) => \
 *   div("ObjValue", `${key} is equal to ${value}`),\
 * ));\
 */
export function rkmap(
  obj: ContainerOrProxy<Object | Array<any>>, 
  renderFn: RkMapFn<any, any>
): ChildNode[] {
  return _anyRMap(true, obj, renderFn);
}

function _anyRMap(
  withKey: boolean,
  obj: ContainerOrProxy<Object | Array<any>>, 
  renderFn: AnyRMapFn<any, any>
): ChildNode[] {
  if (isProxy(obj)) {
    if (withKey) {
      return obj._manager.rkmap(renderFn);
    }
    return obj._manager.rmap(renderFn as any);

  } else if (typeof obj === "object") {
    const out = [];
    let renders: Innards[];
    if (Array.isArray(obj)) {
      renders = obj.map((value, index) => renderFn(value, index));
    } else {
      renders = Object.entries(obj).map(([key, value]) => renderFn(value, key));
    }
    renders.forEach((render) => {
      const nodesList = convertInnardsToElements(render);
      if (!nodesList || !nodesList.length) { return; }
      nodesList.forEach((node) => out.push(node));
    });
    return out;
  }
}


export function isObjectish<TYPE>(checkMe: TYPE): checkMe is ContainerOrProxy<TYPE> {
  if (typeof checkMe !== "object") { return false; }
  if (isProxy(checkMe)) {
    return !!checkMe._manager.isUsingMap();
  }
  return true;
}

export function isArrayish<TYPE>(checkMe: TYPE): checkMe is ContainerOrProxy<TYPE> {
  if (typeof checkMe !== "object") { return false; }
  if (isProxy(checkMe)) {
    return !!checkMe._manager.isUsingArray();
  }
  return Array.isArray(checkMe);
}

export function isProxy<TYPE>(checkMe: TYPE): checkMe is SourceProxy<TYPE> {
  // console.log(checkMe, typeof checkMe);
  return typeof checkMe === "object" && !!(checkMe as any)._manager;
}




