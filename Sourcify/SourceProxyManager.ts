import { SourceMap, SourceArray, ISourceContainer, RkMapFn, AnyRMapFn, RMapFn } from "../Derivations/Sources";
import { Innards } from '../El-Tool/El-Tool';
import { DerivationManager } from '../Derivations';
import { ISourceProxyExits, SourceProxy } from './SourcifyBase';


export type PropName = string | number;
type TargetType = "array" | "object" | undefined;

export class SourceProxyManager<TYPE> implements ISourceContainer<any, any>{
  private keyProxies = new Map<PropName, any>();
  private sourceMap = new SourceMap<PropName, any>();
  private sourceArray = new SourceArray();
  private currentTarget?: TYPE;
  private targetType: TargetType;
  private proxy: TYPE;
  private target: ISourceProxyExits<TYPE> = {
    _manager: this,
    _static: undefined,
  };

  constructor (protected getTarget: () => TYPE) {}

  public getProxy(): SourceProxy<TYPE> {
    return this.proxy = this.proxy || new Proxy(this.target, {
      get: (_, propName) => {
        if (typeof propName === "symbol") { return; }
        this.validateCurrentTarget();
        // proxy escapes
        switch (propName) {
          case "_manager": return this;
          case "_static": return this.currentTarget;
        }
        if (typeof propName === "string" && !isNaN(parseInt(propName))) {
          propName = parseInt(propName);
        }
        // try getting value
        const value = this.get(propName);
        if (value !== undefined) { return value; }
        // otherwise return properties/fns of the array or map
        const sourceFns = this.targetType === "array" ? this.sourceArray : this.sourceMap;
        return sourceFns[propName];
      },
      set: (_, propName, value) => {
        if (typeof propName === "symbol") { return; }
        if (typeof propName === "string" && !isNaN(parseInt(propName))) {
          propName = parseInt(propName);
        }
        this.validateCurrentTarget();
        // don't update if nothing changed
        if (this.currentTarget[propName] === value) { return true; }
        // set prop value, then update based off it
        this.currentTarget[propName] = value;
        this.updateFromTarget(propName);
        return true;
      }
    }) as any;
  }


  public mapKeyedReplacers = this.rkmap;
  public rkmap(renderFn: RkMapFn<any, any>): ChildNode[] {
    return this._mapReplacers(true, renderFn);
  }

  public mapReplacers = this.rmap;
  public rmap(renderFn: RMapFn<any>): ChildNode[] {
    return this._mapReplacers(false, renderFn);
  }

  private _mapReplacers(withKey: boolean, renderFn: AnyRMapFn<any, any>) {
    this.validateProps();
    const container = this.getSourceContainer();
    if (container) {
      if (withKey) {
        return container.rkmap(renderFn);
      }
      return container.rmap(renderFn as any);
    }
  }

  
  protected get(propName: PropName) {
    this.validateProps([propName]);
    if (this.targetType === "object") {
      return this.sourceMap.get(propName);
    } else if (typeof propName === "number") {
      return this.sourceArray.get(propName);
    }
  };



  protected validateCurrentTarget() {
    const newTarget = this.getTarget();
    if (newTarget !== this.currentTarget) {
      const oldTarget = this.currentTarget;
      this.target._static = this.currentTarget = newTarget;

      if (typeof newTarget === "object") {
        this.targetType = Array.isArray(newTarget) ? "array" : "object";
      } else {
        if (newTarget) { console.error("Target is defined", newTarget); }
        this.targetType = undefined;
        return;
      }
      DerivationManager._ignoreSources("batch proxy syncing chore", () => {
        // skip deleting values where possible
        if (this.isUsingMap()) {
          this.sourceMap.keys().forEach((key) => {
            if (newTarget.hasOwnProperty(key) === false) {
              this.sourceMap.delete(key); 
            } else if (newTarget[key] !== oldTarget[key]) {
              this.sourceMap.set(key as any, newTarget[key]);
            }
          });

        } else if (this.isUsingArray()) {
          const newLength = (newTarget as any).length;
          const oldLength = oldTarget ? (oldTarget as any).length : 0;
          for (let i = 0; i < oldLength; i++) {
            if (i < newLength) {
              this.sourceArray.set(i, newTarget[i]);
            } else {
              this.sourceArray.splice(i, oldLength - i);
              return;
            }
          }
        }
      })
    }
  }

  protected validateProps(propNames?: PropName[]) {
    this.validateCurrentTarget();
    if (!this.currentTarget) { return; }
    DerivationManager._ignoreSources("batch proxy syncing chore 2", () => {
      if (this.targetType === "object") {
        propNames = propNames || Object.keys(this.currentTarget);
        propNames.forEach((propName) => 
          this.sourceMap.peekHas(propName) ? null : this.updateFromTarget(propName)
        );
      } else if (this.targetType === "array") {
        propNames = propNames || Array((this.currentTarget as any).length).fill(null).map((_, i) => i);
        propNames.forEach((propName: number) => 
          this.sourceArray.peek(propName) ? null : this.updateFromTarget(propName)
        );
      }
    });
  }


  protected updateFromTarget(propName: PropName) {
    let targetValue = this.currentTarget[propName];
    const currentValue = this.getSourceContainer().peek(propName as any);
    if (currentValue === targetValue) { return; }
    
    // if there's an object value, proxy is required
    if (typeof targetValue === "object") {
      // any access by key will always get the same proxy
      this.assertKeyProxy(propName);  
      targetValue = this.keyProxies.get(propName);

      // static proxy makes array sortable.  doesn't reference target by index
      if (this.isUsingArray()) {
        const staticValue = this.currentTarget[propName];
        const proxyMgmt = new SourceProxyManager<any>(() => staticValue);
        this.sourceArray.set(propName as number, proxyMgmt.getProxy());
        return;
      }
    }
    if (this.isUsingMap()) {
      this.sourceMap.set(propName, targetValue, true);
    } else if (this.isUsingArray(propName)) {
      this.sourceArray.set(propName, targetValue);
    }
  }

  protected assertKeyProxy(propName: PropName) {
    if (!this.keyProxies.has(propName)) {
      const proxyMgmt = new SourceProxyManager<any>(() => {
        this.validateCurrentTarget();
        this.sourceMap.get(propName);  // makes child register for changes to this
        return this.currentTarget && this.currentTarget[propName];
      });
      this.keyProxies.set(propName, proxyMgmt.getProxy());
    }
  }

  private getSourceContainer() {
    switch(this.targetType) {
      case "array": return this.sourceArray;
      case "object": return this.sourceMap;
    }
  }
  public isUsingMap() { return this.targetType === "object"; }
  public isUsingArray(propName?: PropName): propName is number { 
    return this.targetType === "array" && (!propName || typeof propName === "number"); 
  }
}