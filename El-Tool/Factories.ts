import { IProps, Innards, el, standardizeInput } from './El-Tool';

/*****************************
* REGULAR CREATION SHORTHANDS
******************************/

/** Typical first factory arg.  Either className string, or factory props */
export type StandardArg1<PROP> = string | PROP;

/** Typical second factory arg.  Either factory props, or innards to append */
export type StandardArg2<PROP> = PROP | Innards;

/** Typical factory arguments */
export type StandardArgs<PROP = IProps, INNARD = Innards> = [] 
	| []
	| [StandardArg1<PROP>]
	| [StandardArg1<PROP>, StandardArg2<PROP>]
	| [StandardArg1<PROP>, StandardArg2<PROP>, INNARD];

/** Typical factory arguments */
export type NoClassArgs<PROP = IProps, INNARD = Innards> = [] 
	| [StandardArg2<PROP>]
	| [StandardArg2<PROP>, INNARD];

type CoP<PROP = IProps> = StandardArg1<PROP>;
type PoI<PROP = IProps> = StandardArg2<PROP>;
type Inn = Innards;


/** Helper function for any factories which have no special props */
export function _simpleFactory<K extends keyof HTMLElementTagNameMap, PROP = IProps>(
	tagName: K, 
	[arg1, arg2, arg3]: StandardArgs<PROP>,
): HTMLElementTagNameMap[K]  {
	const {props, innards} = standardizeInput(arg1, arg2, arg3);
	return el(tagName, props, innards);
}


/** Creates a div from the StandardArgs.
 * @importance 21 */
export function div(classOrProp?: CoP, propOrChild?: PoI, child?: Innards) {
	return _simpleFactory("div", [classOrProp, propOrChild, child]);
}

/** Creates a <span> element 
 * @importance 14
*/
export function span(...[classOrProp, propOrChild, child]: StandardArgs) {
	return _simpleFactory("span", [classOrProp, propOrChild, child]);
}

/** Creates a <pre> element 
 * @importance 13
*/
export function pre(...[classOrProp, propOrChild, child]: StandardArgs) {
	return _simpleFactory("pre", [classOrProp, propOrChild, child]);
}

/** Creates a <ul> element 
 * @importance 13
*/
export function ul(...[classOrProp, propOrChild, child]: StandardArgs) {
	return _simpleFactory("ul", [classOrProp, propOrChild, child]);
}

/** Creates a <li> element 
 * @importance 13
*/
export function li(...[classOrProp, propOrChild, child]: StandardArgs) {
	return _simpleFactory("li", [classOrProp, propOrChild, child]);
}

/** Factory function a() can take special props
 * @importance 14
 * @prop.href Is the href attribute for an <a href=""> element*/
export interface IAnchorProps extends IProps { 
	href?: string; 
	target?: "_blank";
}
export function a(...[classOrProp, propOrChild, child]: StandardArgs<IAnchorProps>) {
	const {props, innards} = standardizeInput(classOrProp, propOrChild, child);
	props.attr = props.attr || {};
	props.attr.href = props.href || "#";
	if (props.target) { props.attr.target = props.target; }
	return el("a", props, innards);
}

export function extLink(...[classOrProp, propOrChild, child]: StandardArgs<IAnchorProps>) {
	const {props, innards} = standardizeInput(classOrProp, propOrChild, child);
	props.target = "_blank";
	return a(props, innards);
}


export interface IImageProps extends IProps { src: string; }
/** Factory function img() can take special props
 * @importance 14
 * @prop.src Is the src attribute for an <img src=""> element*/
export function img(...[classOrProp, propOrChild, child]: StandardArgs<IImageProps>) {
	const {props, innards} = standardizeInput(classOrProp, propOrChild, child);
	props.attr = props.attr || {};
	props.attr.src = props.src;
	return el("img", props, innards);
}


export interface IButtonProps extends IProps { onPush: () => any; }
/** Factory function button() can take special props
 * @importance 14
 * @prop.onPush Is a function which will get called onTap or onClick*/
export function button(...[classOrProp, propOrChild, child]: StandardArgs<IButtonProps>) {
	return _simpleFactory("button", [classOrProp, propOrChild, child]);
}


/** Factory function textInput() can take special props
 * @importance 13
 * @prop.onValueChange Is a function which will get called whenever the value of the input changes*/
export interface ITextInputProps extends IProps { onValueChange?: (value: string) => any; }
export function textInput(
	classNameOrProps?: string | ITextInputProps,
	propsOrInnards?: ITextInputProps | String,
	maybeInnards?: String,
): HTMLInputElement {
	const {props, innards} = standardizeInput(classNameOrProps, propsOrInnards as any, maybeInnards as any);
	if (props.onValueChange) {
		props.on = props.on || {};
		props.on.change = (ev: Event) => props.onValueChange((ev.target as HTMLInputElement).value);
	}
	const out = el("input", props);
	out.type = "text";
	out.value = innards as string;
	return out;
}



/** Creates a <i> element 
 * @importance 14
*/
export function italic(...[propOrChild, child]: NoClassArgs) {
	return _simpleFactory("i", [null, propOrChild, child]);
}

/** Creates a <b> element 
 * @importance 14
*/
export function bold(...[propOrChild, child]: NoClassArgs) {
	return _simpleFactory("b", [null, propOrChild, child]);
}



/** Create an unmodified hr element 
 * @importance 13
*/
export function hr() { return document.createElement("hr"); }
/** Create an unmodified br element 
 * @importance 14
*/
export function br() { return document.createElement("br"); }


/** Creates a <h1> element 
 * @importance 13
*/
export function h1(...[propOrChild, child]: NoClassArgs) {
	return _simpleFactory("h1", [null, propOrChild, child]);
}

/** Creates a <h2> element 
 * @importance 13
*/
export function h2(...[propOrChild, child]: NoClassArgs) {
	return _simpleFactory("h2", [null, propOrChild, child]);
}

/** Creates a <h3> element 
 * @importance 13
*/
export function h3(...[propOrChild, child]: NoClassArgs) {
	return _simpleFactory("h3", [null, propOrChild, child]);
}

/** Creates a <h4> element 
 * @importance 13
*/
export function h4(...[propOrChild, child]: NoClassArgs) {
	return _simpleFactory("h4", [null, propOrChild, child]);
}

/** Creates a <h5> element 
 * @importance 13
*/
export function h5(...[propOrChild, child]: NoClassArgs) {
	return _simpleFactory("h5", [null, propOrChild, child]);
}

/** Creates a <h6> element 
 * @importance 13
*/
export function h6(...[propOrChild, child]: NoClassArgs) {
	return _simpleFactory("h6", [null, propOrChild, child]);
}






