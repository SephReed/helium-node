import { RerenderReplacer, DerivationManager } from '../Derivations';

/**************************
*. BASIC HELPER FUNCTIONS
***************************/

/** Short for document.getElementById() 
 * @importance 14
 * @returnVar elementOfId */
export function byId(id: string): HTMLElement { return document.getElementById(id); }


/** Short for document.addEventListener("DOMContentLoaded"). 
 * It is recommended you start rendering before the dom is ready, then use this
 * event listener to add your content to the page.
 * @importance 20
 * @eg const appUi = div("App", "I'm the app");\
 * onDomReady(() => document.body.appendChild(appUi)));
*/
export function onDomReady(cb: () => void) {
	if (domIsReady) { cb(); return; }
	window.addEventListener("DOMContentLoaded", cb);
}
let domIsReady = false;
onDomReady(() => domIsReady = true);


/** Acts similar to array.splice, but for an Element.classList. Can add/remove multiple
 * class names at once with either an array or string with spaces (eg. "big red dog")
 * @importance 10
 * @returnVar sameElement
 * @param.removeClasses classes which will be removed
 * @param.addClasses classes which element will have after function runs */
export function classSplice(
	element: Element,
	removeClasses: string | string[] | null,
	addClasses?: string | string[],
): Element {
	if (removeClasses) {
		const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
		remList.forEach((className) => element.classList.remove(className));
	}
	if (addClasses) {
		const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
		addList.forEach((className) => element.classList.add(className));
	}
	return element;
}


/** Short for classSplice(element, null, addClasses).  Can be used to 
 * add a class to something without affecting its render code.
 * @importance 15
 * @returnVar sameElement
 * @eg const sectionUi = renderSection();\
 * div("Eg", addClass("sectionName", sectionUi)) */
export function addClass(addClasses: string | string[], element: Element): Element {
	return classSplice(element, null, addClasses);
}


/** Boolean switcher for addind and removivng classes to Element.classList. 
 * @importance 15
 * @eg haveClass(div, "selected", div === selectedItem)
 * @param.className class or classes to add/remove.  Can be array, or space separated string for groups.
 * @param.addNotRemove whether the element should or should not have these classes. 
 * @returnVar sameElement*/
export function haveClass(element: Element, className: string | string[], addNotRemove: boolean): Element {
  return addNotRemove ? (
    classSplice(element, null, className)
  ) : (
    classSplice(element, className, null)
  );
}


/** Shorthand way to remove all children from an element 
 * @importance 6
*/
export function removeChildren(parent: BaseNode): void {
	let child = parent.firstChild;
	while(!!child) {
		child.remove();
		child = parent.firstChild;
	}
}


/** Shorthand way to set styles on element
 * @importance 11
 * @param.style Can be string to overwrite all inline styles (<a style="...">)
 * or an object using camel case to only overwrite specific properties.
 * @eg setStyle(body, "color: #111; background-color: orange;");\
 * setStyle(body, {\
 *   color: "#111",\
 *   backgroundColor: "orange",\
 * }); */
export function setStyle(root: HTMLElement, style: Partial<CSSStyleDeclaration> | string): void {
	if (typeof style === "string") {
		root.style.cssText = style;
	} else {
		Object.keys(style).forEach((styleName) => root.style[styleName] = style[styleName]);
	}
}

export interface IToucherPosition {
	top: number;
	left: number;
}
export type ToucherMoveEvent = (MouseEvent | TouchEvent) & { positions: Array<IToucherPosition> };
export function onToucherMove(
	root: Element, 
	cb: (ev: ToucherMoveEvent) => EventResponse
) {
	const getPosFromClientXY = (x: number, y: number) => {
		const clientRect = root.getBoundingClientRect();
		return {
			left: x - clientRect.left,
			top: y - clientRect.top,
		};	
	}
	root.addEventListener("mousemove", (ev: MouseEvent) => {
		const toucherMoveEvent = ev as ToucherMoveEvent;
		toucherMoveEvent.positions = [
			getPosFromClientXY(ev.clientX, ev.clientY)
		];
		cb(toucherMoveEvent);
	});
	root.addEventListener("touchmove", (ev: TouchEvent) => {
		const toucherMoveEvent = ev as ToucherMoveEvent;
		const clientRect = root.getBoundingClientRect();
		toucherMoveEvent.positions = Array.from(ev.touches).map((touch) => 
			getPosFromClientXY(touch.clientX, touch.clientY)
		);
		cb(toucherMoveEvent);
		ev.preventDefault();
	}) 
}

export type ToucherEnterExitEvent = (MouseEvent | TouchEvent) & { isEnterEvent: boolean };
export function onToucherEnterExit(
	root: Element, 
	cb: (ev: ToucherEnterExitEvent) => EventResponse
) {
	const modEvent = (ev: any, isEnterEvent: boolean) => {
		ev.isEnterEvent = isEnterEvent;
		return ev as ToucherEnterExitEvent;
	}
	root.addEventListener("mouseenter", (ev) => cb(modEvent(ev, true)));
	root.addEventListener("mouseleave", (ev) => cb(modEvent(ev, false)));
	root.addEventListener("touchstart", (ev) => {
		cb(modEvent(ev, true));
		ev.preventDefault();
	});
	root.addEventListener("touchend", (ev) => {
		cb(modEvent(ev, false));
		ev.preventDefault();
	});
}

/**************************
*. TYPES / INTERFACES
***************************/

export type RawHTML = {hackableHTML: string};
export type BaseNode = ChildNode;

export type StaticInnard = string | BaseNode | number | null | void | undefined | RawHTML;
export type SingleInnard = StaticInnard | (() => StaticInnard | Array<StaticInnard>);

export type ManyStaticInnards = SingleInnard | Array<SingleInnard>;
export type Innards = ManyStaticInnards | (() => ManyStaticInnards);

export type AugmentCb<
	CB_ARGS extends Array<any> = [], 
	CB_RETURN = void
> = (ref: Element, ...args: CB_ARGS) => CB_RETURN;

type EventResponse = boolean | void | any;
export interface IProps<NODE extends BaseNode = HTMLElement> {
	id?: string;
	class?: string;
	// className?: string;
	// classList?: string[];
	title?: string;
	attr?: {[key: string]: string};
	style?: Partial<CSSStyleDeclaration> | string;
	on?: {[key: string]: (event: Event) => EventResponse};
	onPush?: (event: MouseEvent | TouchEvent) => EventResponse;
	onToucherDown?: (event: MouseEvent | TouchEvent) => EventResponse;
	onToucherMove?: (event: ToucherMoveEvent) => EventResponse;
	onToucherEnterExit?: (event: ToucherEnterExitEvent) => EventResponse;
	onClick?: (event: MouseEvent) => EventResponse;
	onMouseDown?: (event: MouseEvent) => EventResponse;
	onKeyDown?: (event: KeyboardEvent) => EventResponse;
	onHoverChange?: (isHovered: boolean, event?: MouseEvent) => EventResponse;
	this?: {
		[key: string]: any;
	}
	innards?: Innards;
	ref?: (ref: NODE) => any;
	ddxClass?: () => string | string[];
	ddx?: (ref: NODE) => any;
}

/**************************
*. TYPES CHECKS
***************************/

/** Check if thing is DOM appendable node 
 * @returnVar checkMeIsNode */
export function isNode(checkMe: any): checkMe is BaseNode {
	return checkMe instanceof Node;
}

/** Check if thing reprensents raw html.  WARNING: Always double check raw html for injection. 
 * @returnVar checkMeIsRawHTML */
export function isRawHTML(checkMe: any): checkMe is RawHTML {
	return typeof checkMe === "object" && checkMe.hasOwnProperty("hackableHTML");
}

/** Check if thing represents rendering props for el-tool 
 * @returnVar checkMeIsProps */
export function isProps(checkMe: any): checkMe is IProps {
	return typeof checkMe === "object" 
		&& !Array.isArray(checkMe) 
		&& !isNode(checkMe) 
		&& !isRawHTML(checkMe);
}



/*************************
*. MAIN CREATION FN
*************************/

/** Main function at the root of el-tool (and Helium).  First standardizes output,
 * into rendering properties and innards-to-append.  Next it creates an HTMLElement
 * and begins modifying it based off the props.  Then it resolves innards to
 * an Element list and appends them as children.  And finally, it returns the HTMLElement
 * it has created, modified, and added children to.
 * @importance 16
 * @param.tagName The name of the element to create (eg. "div" -> <div>).
 * @param.propsOrInnards Either the props by which to modify the element, or innards to append.
 * @param.innards Either the innards to append, or empty.*/
export function el<K extends keyof HTMLElementTagNameMap>(
	tagName: K,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLElementTagNameMap[K];
export function el(
	tagName: string,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLElement {
	const input = standardizeInput(null, propsOrInnards, innards);
	return __createEl(tagName as any, input.props, input.innards);
}

/** Takes standard three arguments and returns a object with unambiguos props and innards.
 * Helper function for el() and factories such as div, span, etc. 
 * @importance 16
 * @returnVar {props, innards}*/
export function standardizeInput<PROPTYPE extends IProps>(
	classNameOrProps?: string | PROPTYPE,
	propsOrInnards?: PROPTYPE | Innards,
	innards?: Innards,
): {
	props: PROPTYPE,
	innards: Innards,
} {
	let props: PROPTYPE;
	if (classNameOrProps && isProps(classNameOrProps)) {
		props = classNameOrProps;
	} else if (propsOrInnards && isProps(propsOrInnards)) {
		props = propsOrInnards;
	} else {
		props = {} as any;
	}

	if (propsOrInnards && !isProps(propsOrInnards)) {
		if (innards !== undefined) { throw new Error("Can not double define innards"); }
		innards = propsOrInnards;
	}

	if (typeof classNameOrProps === "string") {
		props.class = (props.class ? props.class + " " : "") + classNameOrProps;
	}
	return {props, innards};
}


function __createEl<K extends keyof HTMLElementTagNameMap>(
	tagName: K,
	props?: IProps,
	innards?: Innards,
): HTMLElementTagNameMap[K] {
	const out = document.createElement(tagName);

	if (props) {
		// event stuff
		if (props.on) {
			const eventListeners = props.on || {};
			Object.keys(eventListeners).forEach((eventType) =>
				out.addEventListener(eventType, eventListeners[eventType])
			);
		}
		if (props.onClick) { out.addEventListener("click", props.onClick); }
		if (props.onKeyDown) { out.addEventListener("keydown", props.onKeyDown); }
		if (props.onMouseDown) { out.addEventListener("mousedown", props.onMouseDown); }
		if (props.onPush) { out.addEventListener("click", props.onPush); }
		if (props.onToucherDown) { out.addEventListener("mousedown", props.onToucherDown); }
		if (props.onToucherMove) { onToucherMove(out, props.onToucherMove); }
		if (props.onToucherEnterExit) { onToucherEnterExit(out, props.onToucherEnterExit); }
		if (props.onHoverChange) {
			out.addEventListener("mouseover", (ev: MouseEvent) => props.onHoverChange(true, ev));
			out.addEventListener("mouseout", (ev: MouseEvent) => props.onHoverChange(false, ev));
		}

		// element html props, and element js props
		if (props.title) { out.title = props.title; }
		if (props.attr) {
			const attrs = props.attr || {};
			Object.keys(attrs).forEach((attr) => out.setAttribute(attr, attrs[attr]));
		}
		if (props.this) {
			const thisProps = props.this || {};
			Object.keys(thisProps).forEach((prop) => out[prop] = thisProps[prop]);
		}

		// classing
		const classList = [];
		if (props.class) { classList.push(props.class) }
		if (classList.length) {
			out.className = classList.join(" ");
		}
		if (props.id) { out.id = props.id; }


		// inline styles
		if (props.style) { setStyle(out, props.style); }

		// innards alternative
		if (props.innards !== undefined) { 
			append(out, props.innards); 
		}
	}

	// add all chidren, or innards
	if (innards !== undefined) { append(out, innards); }

	if (props.ref) { props.ref(out); }
	if (props.ddxClass) {
		let lastClassList: string | string[];
		const scope = DerivationManager.getnitDeriverScope(() => {
			const removeUs = lastClassList;
			classSplice(out, removeUs, lastClassList = props.ddxClass());
		}); 
		scope.derive();
	}
	if (props.ddx) { 
		const scope = DerivationManager.getnitDeriverScope(() => props.ddx(out)); 
		scope.derive();
	}

	return out;
}


/** Removes all children, then appends innards
 * @related El-Tool-append 
 * @importance 15
 * @eg setInnards(document.body, [\
 *   div("Section", "This is the first section"),\
 *   div("Section", "This is the second section"),\
 * ]);
*/
export function setInnards(root: BaseNode, innards: Innards): void {
	removeChildren(root);
	append(root, innards);
}



/** Appends innards to root element.
 * @importance 14
 * @param.before Optional element to insert before.*/
export function append(
	root: BaseNode,
	innards: Innards,
	before?: ChildNode,
): void {
	const addUs = convertInnardsToElements(innards);
	if (before) {
		addUs.forEach((node) => {
			root.insertBefore(node, before);
		});
	} else {
		addUs.forEach((node) => {
			root.appendChild(node);
		});
	}
}

/** Takes innards and converts them to DOM elements.  If the typeof innards is a function
 * that function will be managed by a RerenderReplacer 
 * @returnVar childNodes */
export function convertInnardsToElements(innards: Innards): ChildNode[] {
	if (innards === undefined || innards === null) { return []; }
	const out: ChildNode[] = [];
	if (typeof innards === "function") {
		const refresher = new RerenderReplacer(innards);
		return refresher.getRender(); 
	}
	let childList = Array.isArray(innards) ? innards : [innards];
	childList.forEach((child) => {
		const nodes = _convertSingleInnardToNodes(child);
		nodes.forEach((node) => out.push(node));
	});
	return out;
}


/** Converts a single item from an innards array into DOM elements 
 * @returnVar childNodes */
function _convertSingleInnardToNodes(child: SingleInnard) {
	if (typeof child === "function") {
		const replacer = new RerenderReplacer(child);
		return replacer.getRender();
		
	} else {
		// return _convertSingleStaticInnardToNodes(child);
		const out: BaseNode[] = [];
		if (child === undefined || child === null) { return []; }
		if (typeof child === "number") { child = child + ""; }
		if (isRawHTML(child)) {
			const template = document.createElement('div');
			template.innerHTML = child.hackableHTML;
			template.getRootNode();
			Array.from(template.childNodes).forEach((child) => out.push(child));
		} else if (typeof child === "string") {
			out.push(new Text(child))
		} else if (child) {
			out.push(child); 
		}
		return out;
	}
}

