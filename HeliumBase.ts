import { RerenderReplacer } from './Derivations';

/** Any object with storage area for helium logic.  Storage area is
 * always called "obj._helium"
 * @prop.rerenderReplacer Replacer placeholders have this property
 * @prop.freeze Function, or list of functions to called when element is frozen
 * @prop.unfreeze Function, or list of functions to called when element is unfrozen
 * @prop.moveMutation Optimization bool, used to ignore add/remove mutations when replacers shuffle items
 * @prop.isReplacer True if element is a replacer placeholder. */
export type IHeliumHazed<BASE> = BASE & {
  _helium: {
    rerenderReplacer?: RerenderReplacer;
    freeze?: () => any;
    unfreeze?: () => any;
    frozen?: boolean;
    moveMutation?: boolean;
    isReplacer?: boolean;
  }
}

/** Add rookie._helium = {} if it doesn't exist already */
export function heliumHaze<BASE>(rookie: BASE) {
  (rookie as any)._helium = (rookie as any)._helium || {};
  return rookie as IHeliumHazed<BASE>;
}

/** Check if thing has storage area for Helium logic */
export function isHeliumHazed<BASE>(checkMe: BASE): checkMe is IHeliumHazed<BASE> {
  return (checkMe as any)._helium;
}
