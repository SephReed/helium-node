import { DeriverScope } from './DeriverScope';
import { Deriver } from './Derivers/DerivationBase';
import { isHeliumHazed, heliumHaze } from '../HeliumBase';
import { SourceId, SourceBase } from './Sources/SourceBase';


/**
 * Makes any function act as a deriver.  Useful for keeping derived values up-to-date.
 * @importance 23
 * @param.deriver A function to call immediately, and then every time any of the sources
 * it used on its last call are modified.
 * @eg const state = Sourcify({\
 *   name: "test",\
 *   nameIsBob: false,\
 * });\
 * derive(() => state.nameIsBob = state.name === "Bob");\
 * state.name = "Bob";\
 * console.log(state.nameIsBob); // outputs true\
 */
export function derive(deriver: Deriver) {
  const scope = DerivationManager.getnitDeriverScope(deriver);
  scope.derive();
}


type LogTypes = "freeze" | "unfreeze" | "sourceUpdated" | "sourceRequest" | "treeMutations" | "dirty" | "scopes";

/** Central manager for derivers and their dependencies on sources.  Houses all  
 * of the major functionality which make source-derivers possible.*/
export const DerivationManager = new (class DerivationManager {
  private currentScope: DeriverScope;
  private scopeStack: DeriverScope[] = [];
  private lastSourceId = 0;
  // private sourceChangeDates = new Map<SourceId, number>();
  private sourceToScopeMap = new Map<SourceId, Map<DeriverScope, void>>();
  private deriverToScopeMap = new Map<Deriver, DeriverScope>();
  private mutationObserver: MutationObserver;
  private ignoringSourceUpdatesReason: string;
  private ignoringSourceRequestsReason: string;

  public showLogging: {[key in LogTypes]?: boolean} = {} || {
    freeze: true,
    unfreeze: true,
    sourceUpdated: true,
    sourceRequest: true,
    treeMutations: true,
    dirty: true,
    scopes: true,
  }
  private logEmojiMap: {[key in LogTypes]: string}  = {
    freeze: "❄️",
    unfreeze: "🔥",
    dirty: "💩",
    sourceUpdated: "📦",
    treeMutations: "🌲",
    scopes: "🔎",
    sourceRequest: "🧲",
  }
  public log(type: LogTypes, ...args: any[]) {
    if (!this.showLogging || !this.showLogging[type]) { return; }
    if (typeof args[0] === "string") {
      args[0] = `${this.logEmojiMap[type]} ${args[0]}`;
    } else {
      args.unshift(this.logEmojiMap[type]);
    }
    console.log(...args);
  }


  /** If there are no sources, then this manager is disabled.  You can skip any logic using it.*/
  public disabled() {
    return this.lastSourceId === 0;
  }


  /** Registers a derivers scope as owning the thread.*/
  public setCurrentDeriver(deriver?: Deriver) {
    if (deriver) {
      this.log("scopes", "New scope", (deriver as any).id);
      this.currentScope = this.getnitDeriverScope(deriver);
    } else {
      this.currentScope = undefined;
    }
  }

  /** Gets the scope manager for a deriver.  If none exists, one is created.*/
  public getnitDeriverScope(deriver: Deriver): DeriverScope {
    if (this.lastSourceId > 0) { this.setupMutationObserver(); }
    const map = this.deriverToScopeMap;
    if (map.has(deriver)) {
      return map.get(deriver); 

    } else {
      const addMe = new DeriverScope(deriver);
      map.set(deriver, addMe);
      return addMe;
    }
  }


  /** Creates a unique source id.  Source ids are strings, and can optionally
   * have text appended to them for a more human readable debugging.  Id's 
   * typically look like this: s32_userName, where s32 means source #32
   * @param.append any text you want to apppend to the id*/
  public requestSourceId(append?: string) {
    if (this.deriverToScopeMap.size > 0) { this.setupMutationObserver(); }
    return `s${this.lastSourceId++}` + (append ? `_${append}` : "");
  }

  /** Registers a source as one of the dependencies of the current running scope (if any).*/
  public sourceRequested(source: SourceBase) { 
    if (!this.currentScope) { return; }
    if (this.ignoringSourceRequestsReason) {
      this.log("sourceRequest", `SILENT: Source requested ${source.getSourceId()}`, { reason: this.ignoringSourceRequestsReason});
      return;
    }
    this.log("sourceRequest", `Source requested ${source.getSourceId()}`);
    this.currentScope.addSourceDependency(source);
  }


  private sourceIdBatch = new Map<SourceBase, void>();
  private deriveAnimationRequest: number;
  /** Triggers any derivers dependent on source.  For the sake of performance, all
   * derivers are updated in batches, grouped together by then next animation frame.
   * Because of this, a deriver can be dependent on multiple layers of updated sources
   * without triggering for each one.
   * @param.loggingValue human readable value of current update.  Used for debugging.*/
  public sourceUpdated(source: SourceBase) {
    if (!!this.ignoringSourceUpdatesReason) { 
      const sourceId = source.getSourceId();
      this.log("sourceUpdated", `SILENT: Sources (${sourceId}) updated:`, {
        value: source.peek(), 
        silenceReason: this.ignoringSourceUpdatesReason
      });
      return; 
    }
    this.sourceIdBatch.set(source);
    if (!this.deriveAnimationRequest) {
      this.deriveAnimationRequest = requestAnimationFrame(() => {
        this.deriveAnimationRequest = undefined;
        const sources = Array.from(this.sourceIdBatch.keys());
        this.sourceIdBatch = new Map();
        if (this.showLogging.sourceUpdated) {
          this.log("sourceUpdated", "<Start Source Update Batch>");
          sources.forEach((source) => 
            this.log("sourceUpdated", `Source (${source.getSourceId()}):`, source.peek())
          );
        }
        const scopes = new Map<DeriverScope, void>();
        sources.forEach((source) => {
          const addScopes = Array.from(this.getScopes(source.getSourceId()).keys());
          addScopes.forEach((addScope) => 
            scopes.has(addScope) ? null : scopes.set(addScope)
          )
        });
        const scopeList = Array.from(scopes.keys());
        this.log("scopes", scopeList);
        scopeList.forEach((scope) => {
          if (scope.deprecated) { return; }
          scope.derive();
        });
      })
    }
  }


  public runAsScope(scope: DeriverScope | null, fn: () => any) {
    this.scopeStack.push(this.currentScope = scope);
    fn();
    this.scopeStack.pop();
    this.currentScope = this.scopeStack.slice(-1)[0];
  }

  
  /** Temporarily detaches thread from source-deriver logic.
   * Any sources modified will not cause updates.
   * @param.reason a short human readable reason, used for debugging
   * @param.whatever logic you wish to run should be put in this function */
  public _ignoreSourceUpdates(reason: string, fn: () => any) {
    this.ignoringSourceUpdatesReason = reason;
    fn();
    this.ignoringSourceUpdatesReason = undefined;
  }


  /** Temporarily detaches thread from source-deriver logic.
   * Any sources requested will not be considered dependencies for current scope.
   * @param.reason a short human readable reason, used for debugging
   * @param.whatever logic you wish to run should be put in this function */
  public _ignoreSourceRequests(reason: string, fn: () => any) {
    this.ignoringSourceRequestsReason = reason;
    fn();
    this.ignoringSourceRequestsReason = undefined;
  }


  /** Temporarily detaches thread from source-deriver logic.
   * Any sources requested will not be considered dependencies for current scope.
   * Any sources modified will not cause updates.
   * @param.reason a short human readable reason, used for debugging
   * @param.whatever logic you wish to run should be put in this function */
  public _ignoreSources(reason: string, fn: () => any) {
    this._ignoreSourceRequests(reason, () => {
      this._ignoreSourceUpdates(reason, fn);
    })
  }


  private getScopes(sourceId: SourceId) {
    const dependencyMap = this.sourceToScopeMap;
    if (dependencyMap.has(sourceId) === false) {
      dependencyMap.set(sourceId, new Map());
    }
    return dependencyMap.get(sourceId);
  }


  public removeScopeReferences(sourceIds: SourceId[], scope: DeriverScope) {
    if (!sourceIds || !sourceIds.length) { return; }
    sourceIds.forEach((sourceId) => {
      const list = this.sourceToScopeMap.get(sourceId);
      if (!list) { return; }
      list.delete(scope);
    });
  }


  public regisetScopeReferences(sourceIds: SourceId[], scope: DeriverScope) {
    sourceIds.forEach((sourceId) => {
      this.getScopes(sourceId).set(scope)
    });
  } 


  public deprecateDeriver(deriver: Deriver) {
    const scope = this.deriverToScopeMap.get(deriver);
    scope.deprecated = true;
    this.deriverToScopeMap.delete(deriver); 
  }


  private mutationsBuffer: MutationRecord[] = [];
  private bufferTimeout: any;
  private setupMutationObserver() {
    if (this.mutationObserver) { return; }
    this.mutationObserver = new MutationObserver((mutationsList) => {
      mutationsList.forEach((mutation) => {
        if (mutation.type !== 'childList') { return; }
        this.mutationsBuffer.push(mutation);
      })
      if (this.bufferTimeout) { clearTimeout(this.bufferTimeout); }
      this.bufferTimeout = setTimeout(() => {
        this.bufferTimeout = undefined;
        const buffer = this.mutationsBuffer;
        this.mutationsBuffer = [];
        const haveNode = new Map<Node, boolean>();
        
        for (const mutation of buffer) {
          mutation.removedNodes.forEach((node) => haveNode.set(node, false));
          mutation.addedNodes.forEach((node) => haveNode.set(node, true));
        }
        const addedNodes: Node[] = [];
        const removedNodes: Node[] = [];
        // const frozenIs = (node: Node, value: boolean) => isHeliumHazed(node) && (node._helium.frozen === value);
        // addedNodes = addedNodes.filter((it) => !frozenIs(it, false));
        // removedNodes = removedNodes.filter((it) => !frozenIs(it, true));
        Array.from(haveNode.entries()).forEach(([node, shouldHave]) => {
          if (isHeliumHazed(node) && node._helium.moveMutation) {
            return node._helium.moveMutation = false;
          }
          shouldHave ? addedNodes.push(node) : removedNodes.push(node);
        });
        this.log("treeMutations", "Adding:", addedNodes);
        this.log("treeMutations", "Removing:", removedNodes);

        addedNodes.forEach((node: Node) => {
          heliumHaze(node);
          this.permeate(node, (child) => {
            if (isHeliumHazed(child)) {
              // if (child._helium.frozen === false) { return; }
              if (child._helium.unfreeze) {
                this.log("unfreeze", '%cUnfreezing item', [
                  "background: linear-gradient(to left, #00a7ff, #0c72da)",
                  "color: black",
                  "padding: 2px",
                ].join(";"), child);
                child._helium.unfreeze();
              }
              child._helium.frozen = false;
            } 
          })
        });

        removedNodes.forEach((node) => {
          heliumHaze(node);
          this.permeate(node, (child) => {
            if (isHeliumHazed(child)) {
              // if (child._helium.frozen) { return; }
              if (child._helium.freeze) {
                child._helium.freeze();
              }
              child._helium.frozen = true;
            }
          })
        });
      }, 10);
    });

    var config = { childList: true, subtree: true };
    this.mutationObserver.observe(document.body, config);
  }


  private permeate(root: Node, cb: (node: Node) => any) {
    cb(root);
    root.childNodes.forEach((child) => this.permeate(child, cb));
  }
})();
console.log(DerivationManager);
