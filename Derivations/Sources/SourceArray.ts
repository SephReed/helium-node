import { Source } from './Source';
import { ISourceContainer } from './SourceBase';
import { SourceMapBase } from './SourceMap';
import { Innards } from '../../El-Tool/El-Tool';
import { DerivationManager } from '../DerivationManager';
import { RerenderReplacer } from '../Derivers/RerenderReplacer';


export class SourceArrayBase<TYPE> {
  protected _array: Source<TYPE>[];
  protected _length: Source<number>;

  constructor(array?: TYPE[]) {
    if (array) {
      this._array = array.map((value) => new Source(value, {idAppend: "aval"}));
      this._length = new Source(array.length, {idAppend: "alen"});
    } else {
      this._array = [];
      this._length = new Source(0, {idAppend: "alen"});
    }
  }

  public peek(index: number) {
    if (typeof index === "number" && this._array[index]) {
      return this._array[index].peek();
    }
  }

  protected getObservable(index: number) {
    if (!this._array[index]) {
      this._array[index] = new Source(undefined, {idAppend: "aval"});
    }
    return this._array[index];
  }

  // gets an item which might change in index
  public getItem(index: number) {
    return this.getObservable(index).get();
  }
  
  public set(index: number, value: TYPE) {
    if (!this._array[index]) {
      this._array[index] = new Source(value, {idAppend: "aval"});
    } else {
      this._array[index].set(value);
    }
    if (index + 1 > this._length.get()) {
      this._length.set(index + 1);
    }
  }

  public push(value: TYPE) {
    const index = this._length.get();
    this.set(index, value);
  }

  public pop(): TYPE {
    const index = this._length.get();
    this._length.set(index - 1);
    const value = this.getItem(index);
    this.set(index, undefined);
    return value;
  }

  public length(): number {
    return this._length.get();
  }
}








/**
 * @importance 7
 */
export class SourceArray<TYPE> 
extends SourceArrayBase<TYPE> 
implements ISourceContainer<number, TYPE> {
  private lastUpdate = new Source<number>(undefined, {idAppend: "aupt"});
  private indexMap: SourceMapBase<number, Source<TYPE>>;

  public splice(start: number, deleteCount?: number, ...items: TYPE[]) {
    if (start < 0) { throw "TODO"}
    const length = this.length();
    start = Math.min(start, length);
    const numDeleted = Math.min(length - start, deleteCount);
    const numAdded = items.length;
    this._array.splice(start, deleteCount, ...items.map((value) => new Source(value)));
    console.log({numAdded, numDeleted});
    this._length.set(Math.max(start, length) + (numAdded - numDeleted));
    this.lastUpdate.set(Date.now());
  }

  public reverse() {
    const activeValues = this._array.splice(0, this.length());
    this._array.splice(0, 0, ...activeValues.reverse());
    this.lastUpdate.set(Date.now());
  }

  // gets an item which is coupled with the index arg
  public get(index: number): TYPE {
    return this._getIndexObserver(index).get().get();
  }

  private _getIndexObserver(index: number) {
    if (!this.indexMap) {
      console.log("Creating index map, slightly less efficient");
      this.indexMap = new SourceMapBase(null, {id: "ai"});
      const scope = DerivationManager.getnitDeriverScope(() => {
        const updateTime = this.lastUpdate.get();
        const length = this._length.get();
        Array.from(this.indexMap._static().keys()).forEach((key) =>
          this.indexMap.set(key, this.getObservable(key))
        );
      });
      scope.derive();
    }
    if (!this.indexMap._static().has(index)) {
      DerivationManager._ignoreSources("Array indexMap initting", () => {
        this.indexMap.set(index, this.getObservable(index));
      })
    }
    return this.indexMap.getObservable(index);
  }


  public mapReplacers = this.rmap;
  public rmap(renderFn: (value: TYPE) => Innards) {
    return this._mapReplacers(false, renderFn);
  }

  public mapKeyedReplacers = this.rkmap;
  public rkmap(renderFn: (value: TYPE, index: number) => Innards) {
    return this._mapReplacers(true, renderFn);
  }

  private _mapReplacers(
    withIndex: boolean, 
    renderFn: (value: TYPE, index?: number) => Innards
  ) {
    const obsToRerenderMap = new Map<Source<TYPE | Source<TYPE>>, RerenderReplacer>();
    const listReplacer = new RerenderReplacer(() => {
      this.lastUpdate.get();
      const out: ChildNode[] = [];
      const length = this.length();
      for (let i = 0; i < length; i++) {
        const observable = withIndex ? this._getIndexObserver(i) : this.getObservable(i);
        if (obsToRerenderMap.has(observable) === false) {
          obsToRerenderMap.set(observable, new RerenderReplacer(() => {
            const value = observable.get();
            if (withIndex) {
              return renderFn((value as Source<TYPE>).get(), i);
            } else {
              return renderFn(value as TYPE);
            }
          }));
        }
        const nodes = obsToRerenderMap.get(observable).getRender();
        nodes.forEach((node) => out.push(node));
      }
      return out;
    });
    return listReplacer.getRender();
  }
}