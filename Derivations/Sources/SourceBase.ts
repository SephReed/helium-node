import { Innards } from '../../El-Tool/El-Tool';

/** A unique id for a source, used by DerivationManager for dependency mapping. */
export type SourceId = string;

export type Setter<TYPE> = (value: TYPE) => void;

export type RMapFn<TYPE> = (value: TYPE) => Innards;
export type RkMapFn<TYPE, KEY> = (value: TYPE, key: KEY) => Innards;
export type AnyRMapFn<TYPE, KEY> = RMapFn<TYPE> | RkMapFn<TYPE, KEY>;

export interface ISourceContainer<KEY, TYPE> {
  rmap: (renderFn: RMapFn<TYPE>) => ChildNode[];
  mapReplacers: (renderFn: RMapFn<TYPE>) => ChildNode[];

  rkmap: (renderFn: RkMapFn<TYPE, KEY>) => ChildNode[];
  mapKeyedReplacers: (renderFn: RkMapFn<TYPE, KEY>) => ChildNode[];
}





export interface SourceBase<VALUE = any> {
  peek: () => VALUE;
  get: () => VALUE;
  getSourceId: () => SourceId;
}

