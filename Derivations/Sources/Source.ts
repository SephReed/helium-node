import { SourceBase, SourceId } from './SourceBase';
import { DerivationManager } from '..';


/** Args object which can be passed to new observables
 * @prop.ignoreSameValues Defaults to true. Bool for whether or not observers should be notified when oldValue === newValue.
 * @prop.idAppend A string to append the end of the unique sourceId for this source. */
export interface ISourceArgs {
  ignoreSameValues?: boolean;
  idAppend?: string;
}


/** Wraps and manages any single value as a Source. If you'd like to have more grainular control over the way
 * a single Source acts, use the new Source() constructor instead.
 * @importance 25
 * @param.value The starting value
 * @param.args Specific instructions about how this observable should act 
 * @eg const numSrc = source(42);\
 * derive(() => console.log(numSrc.get()); // output: 42\
 * numSrc.set(1337);  // output: 1337 */
export function source<VALUE>(value: VALUE) {
  return new Source(value);
}


/** This is the default source type in Helium.  This class is mostly just a wrapper
 * around a value which notifies the DerivationManager when accessed or updated.
 * @importance 7
 * @param.value The starting value
 * @param.args Specific instructions about how this observable should act */
export class Source<VALUE> implements SourceBase<VALUE> {
  private sourceId: SourceId;

  constructor(protected value?: VALUE, protected args: ISourceArgs = {}) {
    this.sourceId = DerivationManager.requestSourceId(args.idAppend);
  }

  /** Returns the current value, and notifies DerivationManager so
   * it can mark the current scope as dependent on this source. */
  public get() {
    DerivationManager.sourceRequested(this);
    return this.value;
  }

  /** Used to get a value without letting DerivationManager know.  
   * An alternative to DerivationManager._ignoreSourceRequests() */
  public peek() {
    return this.value;
  }

  /** Gets the unique id for this source.  Used to allow garbage collection. */
  public getSourceId() {
    return this.sourceId;
  }

  /** Update the current value, then dispatch notifications for updates (generally to DerivationManager)
   * @param.value New value to set to.
   * @param.forceUpdates An override for the default ignoreSameValues setting */
  public set(value: VALUE, forceUpdates?: boolean) {
    if (!forceUpdates && this.args.ignoreSameValues !== false && this.value === value) { return; }
    this.value = value;
    this.dispatchUpdateNotifications();
  }



  /** Iterates over every function registered as an observer, passing in the new value.
   * Then it tells the DerivationManager to rederive dependent derivers.  Unless
   * args.makeObserversCodependent is true, any errors that occur are caught and output to console
   * without breaking flow. */
	protected dispatchUpdateNotifications() {
    DerivationManager.sourceUpdated(this);
	}
}