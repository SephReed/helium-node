import { Innards } from '../../El-Tool/El-Tool';
import { RerenderReplacer } from '..';
import { Source, ISourceArgs } from './Source';
import { ISourceContainer, RkMapFn, AnyRMapFn } from './SourceBase';



export type ReplacerMappingFn<KEY, VALUE> = ([key, value]: [KEY, VALUE]) => Innards;

/**************************
 * OBSERVABLE MAP */

export interface ISourceMapArgs {
  id: string,
}

export class SourceMapBase<KEY, VALUE> {
  protected _map = new Map<KEY, Source<VALUE>>();

  constructor(entries?: Array<[KEY, VALUE]>, private args?: ISourceMapArgs) {
    if (entries) { throw "TODO"; }
  }

  public getObservable(key: KEY) {
		if (!this._map.has(key)) {
      let idAppend = "mval";
      if (this.args && this.args.id) { idAppend = `${this.args.id}_${idAppend}`; }
      if (typeof key === "string" || typeof key === "number") { idAppend += `_${key}`; }
			this._map.set(key, new Source(undefined, {idAppend}));
		}
		return this._map.get(key);
	}

  public get(key: KEY): VALUE {
    return this.getObservable(key).get();
  }
  
  public set(key: KEY, value: VALUE, forceUpdates?: boolean) {
    return this.getObservable(key).set(value, forceUpdates);
  }

  public _static() {
    return this._map;
  }

  public peek(key: KEY): VALUE {
    if (this._map.has(key)) {
      return this._map.get(key).peek();
    }
  }
}




/**
 * @importance 7
 */
export class SourceMap<KEY, VALUE> 
extends SourceMapBase<KEY, VALUE>
implements ISourceContainer<KEY, VALUE> {
  private _keys = new Source<KEY[]>([], {
    idAppend: "mapKeys",
  });
  private _has = new SourceMapBase<KEY, boolean>(null, {id: "has"});

	public set(key: KEY, value: VALUE, forceUpdates?: boolean) {
    let keysChanged = false;
    if (!this.peekHas(key)) { keysChanged = true; }
    this._has.set(key, true);
    super.set(key, value, forceUpdates);
    if (keysChanged) {
      this.updateKeys();
    }
	}

	public delete(key: KEY) {
    if (!this.has(key)) { return; }
    this._has.set(key, false);
    super.set(key, undefined);
    this.updateKeys();
  }
  
  public deleteAll() {
    this.keys().forEach((key) => this.delete(key));
  }

  public getnit(key: KEY, value: VALUE): VALUE {
    if (this.has(key)) { return this.get(key); }
    this.set(key, value);
    return value;
  }


	// secondary functions
	public has(key: KEY) { 
    return !!this._has.get(key); 
  }
	public keys() { return this._keys.get(); }
  public isEmpty() { return !this._keys.get().length; }
  
  public mapReplacers = this.rkmap;
  public rmap(renderFn: RkMapFn<VALUE, KEY>): ChildNode[] {
    return this._mapReplacers(false, renderFn);
  }

  public mapKeyedReplacers = this.rkmap;
  public rkmap(renderFn: RkMapFn<VALUE, KEY>): ChildNode[] {
    return this._mapReplacers(true, renderFn);
  }

  private _mapReplacers(withKey: boolean, renderFn: AnyRMapFn<VALUE, KEY>): ChildNode[] {
    const entryReplacers = new Map<KEY, RerenderReplacer>();
    const orderer = new RerenderReplacer(() => {
      const out: ChildNode[] = [];
      const keys = this.keys();
      for (const key of keys) {
        if (entryReplacers.has(key) === false) {
          const source = this.getObservable(key);
          entryReplacers.set(key, new RerenderReplacer(() => 
            renderFn(source.get(), withKey ? key : undefined)
          ));
        }
        const nodeList = entryReplacers.get(key).getRender();
        for (const node of nodeList) {
          out.push(node);
        }
      }
      return out;
    });
    return orderer.getRender();
  }

  
  private updateKeys() {
    this._keys.set(Array.from(this._map.keys()).filter((key) => this.has(key)));
  }


  public peekHas(key: KEY): boolean {
    return !!this._has.getObservable(key).peek();
  }
}

