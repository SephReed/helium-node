import { Deriver, isDeriverClass } from './Derivers/DerivationBase';
import { DerivationManager } from './DerivationManager';
import { SourceBase, SourceId } from './Sources/SourceBase';


/** A book-keeping class which keeps track of the relationship 
 * between a deriver and the sources it depends on. Typically,
 * only used by DerivationManager.*/
export class DeriverScope {
  private sourceDeps = new Map<SourceBase, void>();
  /** Whether or not this scope is deprecated and can have its
   * references dropped (allowing garbage collection).
   * Typically, scopes are deprecated when they're no longer on the page.*/
  public deprecated = false;

  // @param/deriver - either a function or something with a derive() function
  constructor(private deriver: Deriver){}


  /** Marks this scope as being dependent on a source. */
  public addSourceDependency(source: SourceBase) {
    this.sourceDeps.set(source);
  }

  /** Runs whatever deriver it was created with.  Then passes old and
   * new dependecies to DerivationManager so it can update the source-to-scope map. */
  public derive() {
    DerivationManager.removeScopeReferences(this.getSourceIds(), this);
    DerivationManager.runAsScope(this, () => {
      this.sourceDeps = new Map();
      if (isDeriverClass(this.deriver)) {
        this.deriver.derive();
      } else {
        this.deriver();
      }
    })
    DerivationManager.regisetScopeReferences(this.getSourceIds(), this);
  }


  /** Outputs a unique list of current dependencies. */
  public getSourceDependencies(): SourceBase[] {
    return Array.from(this.sourceDeps.keys());
  }

  private getSourceIds() {
    return Array.from(this.sourceDeps.keys()).map((source) => source.getSourceId());
  }
}
