/** Any type of function used to derive. */
export type DeriveFn = () => any;

/** An abstract class for any class for managing a deriver function.
 * Useful for when you want to keep track of things between derivations.
 * See RerenderReplacer for an example. 
 * @importance 7 */
export abstract class DeriverClass {
  /** The function which will be called every time any sources are updated. */
  public abstract derive(): void;
}

/** Either a function, or a class with a this.derive() function */
export type Deriver = DeriveFn | DeriverClass;


/** Type checker for a DeriverClass. */
export function isDeriverClass(checkMe: any): checkMe is DeriverClass {
  return !!checkMe.derive;
}
