import { Innards, convertInnardsToElements, el, BaseNode } from '../../El-Tool/El-Tool';
import { DerivationManager } from '../DerivationManager';
import { heliumHaze, isHeliumHazed } from '../../HeliumBase';

const REMOVE_NODE = false;
const ADD_NODE = true;

/** A form of deriver management.  Beyond hosting the deriver function, this class 
 * also handles freezing and unfreezing derivation when its placeholder is added
 * or removed from the page.  Further, it makes sure to only add, remove, or swap
 * the elements in each rerender if they require it. 
 * @param.rerenderFn - A function which returns whatever the most up to date elements
 * for it should be */
export class RerenderReplacer {
  private static nextId = 0;
  private currentRender: ChildNode[];
  private id = "ddx_" + RerenderReplacer.nextId++;
  private placeHolder: Comment;
  private dirty = true;
  private frozen = false;

  constructor(protected rerenderFn: () => Innards) {
    this.preConstructHook();
    if (DerivationManager.disabled()) {
      this.currentRender = convertInnardsToElements(this.rerenderFn());
      
    } else {
      let scope = DerivationManager.getnitDeriverScope(this);
      this.placeHolder = document.createComment(this.id);
      const helium = heliumHaze(this.placeHolder)._helium;
      helium.rerenderReplacer = this;
      helium.isReplacer = true;
      helium.freeze = () => this.frozen = true;
      helium.unfreeze = () => {
        if (!this.frozen) {
          console.error("Unfreeze on not frozen placeholder");
        }
        this.frozen = false;
        if (this.dirty) { 
          console.log("💩 unfreeze is dirty", this.placeHolder);
          scope = scope.deprecated ? DerivationManager.getnitDeriverScope(this) : scope;
          scope.derive(); 
        }
      }
      scope.derive();
    }
    this.frozen = true;
  }

  protected preConstructHook() {}

  /** Returns the current render. This render is live and will update automatically. */
  public getRender(): BaseNode[] { return this.currentRender; }

  /** Typically called by DerivationManager, if not frozen, this function 
   * will update its current render both on the page and locally, then return it. */
  public derive(): BaseNode[] {
    // console.log(`Rederiving (${this.id})`, this.currentRender)
    if (this.frozen) {
      this.dirty = true;
      console.log("💩 Deriver is frozen (❄️) marked dirty", this.placeHolder);
      DerivationManager.deprecateDeriver(this);
      return;
    }

    this.dirty = false;
    const newRender = convertInnardsToElements(this.rerenderFn());
    newRender.push(this.placeHolder);
    if (this.currentRender) {
      const placehold = this.placeHolder;
      const addRemoveMap = new Map<Node, boolean>();
      this.currentRender.forEach((node) => addRemoveMap.set(node, false));
      newRender.forEach((node) => {
        if (addRemoveMap.has(node)) {
          addRemoveMap.delete(node)
        } else {
          addRemoveMap.set(node, ADD_NODE);
        }
      });
      Array.from(addRemoveMap.entries()).forEach(([node, addRemove]) => {
        if (addRemove === REMOVE_NODE && node.parentElement) {
          node.parentElement.removeChild(node);
          if (isHeliumHazed(node) && node._helium.isReplacer) {
            node._helium.rerenderReplacer.removeAll();
          }
        }
      });

      const parent = placehold.parentElement;
      if(!parent) {
        console.log(placehold);
        throw "Render placeholder root not on page";

      } else if(placehold.getRootNode() !== document) {
        console.log(placehold);
        throw "Unfrozed placeholder";
      }

      const currentNodes = parent.childNodes;
      let parentIndex = Array.from(currentNodes).indexOf(placehold);
      // console.log("parent index", parentIndex, parent);
      let lastNode: ChildNode;
      const length = newRender.length;
      for (let i = 0; i < length; i++) {
        const addMe = newRender[length - 1 - i];
        const current = currentNodes[parentIndex - i];
        if (addMe === current) {
          // console.log("Same items", addMe, current);
          // do nothing
        } else {
          parent.insertBefore(addMe, lastNode);
          if (addRemoveMap.get(addMe) !== true) {
            // console.log("Non-new item", addMe, current);
            heliumHaze(addMe)._helium.moveMutation = true;
          } else {
            // console.log("New item", addMe, current);
            parentIndex++;
          }
        }
        lastNode = addMe;
      }
    }
    this.currentRender = newRender;
  }

  /** Removes all elements managed by this replacer from the page */
  public removeAll() {
    this.currentRender.forEach((node) => {
      if (node.parentElement) {
        node.parentElement.removeChild(node);
      }
    });
  }
}
