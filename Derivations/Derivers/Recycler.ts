import { Innards } from '../../El-Tool/El-Tool';
import { RerenderReplacer } from './RerenderReplacer';
import { Source, SourceBase } from '../Sources';
import { DerivationManager } from '../DerivationManager';



// export function newRecycle(renderFn: () => Innards) {
//   const renders: Array<Map<SourceBase, any>> = [];

// }

interface ICachedRender {
  sourceVals: Map<SourceBase, any>,
  render: Innards
};


export class RenderRecycler extends RerenderReplacer {
  protected preConstructHook() {
    const renders: Array<ICachedRender> = []
    const rootRenderFn = this.rerenderFn;
    this.rerenderFn = () => {
      let cachedRender = renders.find((prevRender) => {
        const sourceVals = Array.from(prevRender.sourceVals.entries());
        for (const [source, val] of sourceVals) {
          if (val !== source.peek()) { return false; }
        }
        // make sure dependencies are updated
        for (const [source, val] of sourceVals) { source.get(); }
        return true;
      });

      if (!cachedRender) {  
        renders.push(cachedRender = {
          sourceVals: new Map(), 
          render: rootRenderFn(),
        });
        const sources = DerivationManager.getnitDeriverScope(this).getSourceDependencies();
        sources.forEach((source) => cachedRender.sourceVals.set(source, source.peek()));
      } else {
        // console.log("Reusing cached render!", cachedRender);
      }

      return cachedRender.render;
    }
  }
}

/** Shorthand for creating a new RenderRecycler class.  Use this when you want to 
 * cache previous renders.
 * @importance 17
 * @warning This functions stops garbage collection of old views by caching them.  Only use this
 * function for sections of your UI which have very few permutations each with large amounts of content.
 * @eg const view = source("bert");\
 * const viewEl = div("View", recycle(\
 *   // each of these will only be rendered once\
 *   switch (view.get()) {\
 *     case "bert": return renderLargeMarkdown("berts-autobiography.md");\
 *     case "ernie": return renderLargeMarkdown("ernies-autobiography.md");\
 *   }\
 * ));
 */
export function recycle(renderFn: () => Innards) {
  const replacer = new RenderRecycler(renderFn);
  return replacer.getRender();
}



// export class Recycler extends RerenderReplacer {
//   constructor(
//     renderFn: (bin: RecyleBin) => Innards, 
//     private bin = new RecyleBin()
//   ) {
//     super(() => renderFn(bin))
//   }
// }

// type PrevRenderMap = Map<any, Innards>;
// export class RecyleBin {
//   private prevRenders: PrevRenderMap = new Map();

//   public getId(id: any, renderFn: () => Innards) {
//     if (this.prevRenders.has(id) === false) {
//       this.prevRenders.set(id, renderFn());
//     }
//     return this.prevRenders.get(id);
//   }
// }


// export function recycle(renderFn: (bin: RecyleBin) => Innards) {
//   const recycle = new Recycler(renderFn);
//   return recycle.getRender();
// }

// export function recycleKey(getKey: () => any, renderFn: () => Innards) {
//   const recycle = new Recycler((bin) =>
//     bin.getId(getKey(), renderFn),
//   );
//   return recycle.getRender();
// }