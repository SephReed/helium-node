// public showLogging: {[key in LogTypes]?: boolean} = {} || {
//   freeze: true,
//   unfreeze: true,
//   sourceUpdated: true,
//   sourceRequest: true,
//   treeMutations: true,
//   dirty: true,
//   scopes: true,
// }
// private logEmojiMap: {[key in LogTypes]: string}  = {
//   freeze: "❄️",
//   unfreeze: "🔥",
//   dirty: "💩",
//   sourceUpdated: "📦",
//   treeMutations: "🌲",
//   scopes: "🔎",
//   sourceRequest: "🧲",
// }
// public log(type: LogTypes, ...args: any[]) {
//   if (!this.showLogging || !this.showLogging[type]) { return; }
//   if (typeof args[0] === "string") {
//     args[0] = `${this.logEmojiMap[type]} ${args[0]}`;
//   } else {
//     args.unshift(this.logEmojiMap[type]);
//   }
//   console.log(...args);
// }


const logPrepends: {[key: string]: string} = {};
const showLogTypes: {[key: string]: boolean} = {};

export function showLoggingType(type: string, shouldShow: boolean = true) {
  showLogTypes[type] = shouldShow;
}

/**
 * Stands for "another logger."  Because source-derivers are more suited to being a language
 * level feature than a framework, the only way to implement Sourcify was to use Proxies.
 * The unfortunate side effect is that using console.log() yields ugly results.  To avoid this,
 * use aLog.  This function also comes with a categorization feature that can be enabled
 * with showLoggingType()
 * @importance 20
 * @param.args The things you would like to log.  If showLoggingType() is enabled for the string
 * matching the first argument, the log will prepend any defined prependStrings, or block the logging
 * if showLoggingType() is set to false for that log category.
 */
export function aLog(...args: any[]) {
  args = args.map((arg) => 
    (typeof arg === "object" && arg._static) || arg
  );
  if (args.length <= 1) { 
    return console.log(args[0]); 
  }
  const type = args[0];
  const shouldShow = showLogTypes[type];
  if (shouldShow === undefined) { 
    return console.log(...args); 
  } else if (!shouldShow) {
    return;
  }
  const prepend = logPrepends[type];
  if (prepend) {
    if (typeof args[0] === "string") {
      args[0] = `${this.logEmojiMap[type]} ${args[0]}`;
    } else {
      args.unshift(this.logEmojiMap[type]);
    }
  }
  console.log(...args);
}